# Timber Project

This project follows the C++ tutorial *Beginning C++ Game Programming Part 1* from *Packt Publishing*. 

https://www.packtpub.com/game-development/beginning-c-game-programming-part-1-video

It is a great tutorial to get you going.