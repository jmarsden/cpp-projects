#include "stdafx.h"
#include <sstream>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

using namespace sf;

void updateBranches(int seed);

const int NUM_BRANCHES = 6;
Sprite branches[NUM_BRANCHES];

enum class side { LEFT, RIGHT, NONE };
side branchPositions[NUM_BRANCHES];

int main() {

	Vector2f resolution;
	resolution.x = (float) VideoMode::getDesktopMode().width;
	resolution.y = (float) VideoMode::getDesktopMode().height;
	
	Vector2f scaleFactor;
	scaleFactor.x = resolution.x / 1920;
	scaleFactor.y = resolution.y / 1080;

	// Create and Open a Window
	RenderWindow window(VideoMode((int) resolution.x, (int) resolution.y), "Timber!!", Style::Fullscreen);

	// Create the Background Sprite
	Texture textureBackground;
	textureBackground.loadFromFile("graphics/background.png");
	Vector2u textureBackgroundResoultion = textureBackground.getSize();
	Sprite spriteBackground;
	spriteBackground.setTexture(textureBackground);
	spriteBackground.setPosition(0, 0);
	spriteBackground.setScale(resolution.x/textureBackgroundResoultion.x, resolution.y/ textureBackgroundResoultion.y);

	// Create the Tree Sprite
	Texture textureTree;
	textureTree.loadFromFile("graphics/tree.png");
	Vector2u textureTreeResoultion = textureTree.getSize();
	Sprite spriteTree;
	spriteTree.setTexture(textureTree);
	spriteTree.setPosition(810 * scaleFactor.x, 0 * scaleFactor.y);
	spriteTree.setScale(scaleFactor.x, scaleFactor.y);

	// Create the Bee Sprite
	Texture textureBee;
	textureBee.loadFromFile("graphics/bee.png");
	Vector2u textureBeeResoultion = textureBee.getSize();
	Sprite spriteBee;
	spriteBee.setTexture(textureBee);
	spriteBee.setPosition(0 * scaleFactor.x, 800 * scaleFactor.y);
	spriteBee.setScale(scaleFactor.x, scaleFactor.y);

	bool beeActive = false;
	float beeSpeed = 0.0f;

	// Create the Cloud Sprites
	Texture textureCloud;
	textureCloud.loadFromFile("graphics/cloud.png");
	Sprite spriteCloud1;
	Sprite spriteCloud2;
	Sprite spriteCloud3;
	spriteCloud1.setTexture(textureCloud);
	spriteCloud2.setTexture(textureCloud);
	spriteCloud3.setTexture(textureCloud);
	spriteCloud1.setPosition(0 * scaleFactor.x, 0 * scaleFactor.y);
	spriteCloud2.setPosition(0 * scaleFactor.x, 250 * scaleFactor.y);
	spriteCloud3.setPosition(0 * scaleFactor.x, 500 * scaleFactor.y);
	spriteCloud1.setScale(scaleFactor.x, scaleFactor.y);
	spriteCloud2.setScale(scaleFactor.x, scaleFactor.y);
	spriteCloud3.setScale(scaleFactor.x, scaleFactor.y);

	bool cloud1Active = false;
	bool cloud2Active = false;
	bool cloud3Active = false;

	float cloud1Speed = 0.0f;
	float cloud2Speed = 0.0f;
	float cloud3Speed = 0.0f;

	Clock clock;

	RectangleShape timeBar;
	float timeBarStartWidth = 400;
	float timeBarHeight = 80;

	timeBar.setSize(Vector2f(timeBarStartWidth, timeBarHeight));
	timeBar.setFillColor(Color::Red);
	timeBar.setPosition((resolution.x / 2) - timeBarStartWidth / 2, resolution.y - timeBarHeight);
	timeBar.setScale(scaleFactor.x, scaleFactor.y);

	Time gameTimeTotal;
	float timeRemaining = 6.0f;
	float timeBarWidthPerSecond = timeBarStartWidth / timeRemaining;

	bool paused = true;

	int score = 0;
	Text messageText;
	Text scoreText;

	Font font;
	font.loadFromFile("fonts/KOMIKAP_.ttf");

	messageText.setFont(font);
	scoreText.setFont(font);

	messageText.setString("Press Enter to Begin!");
	scoreText.setString("Score: 0");

	messageText.setCharacterSize(75);
	scoreText.setCharacterSize(100);

	messageText.setFillColor(Color::White);
	scoreText.setFillColor(Color::White);

	FloatRect textRect = messageText.getLocalBounds();
	messageText.setOrigin(
		textRect.left + textRect.width / 2.0f,
		textRect.top + textRect.height / 2.0f);
	messageText.setPosition(resolution.x / 2.0f, resolution.y / 2.0f);
	messageText.setScale(scaleFactor.x, scaleFactor.y);
	scoreText.setPosition(20, 20);
	scoreText.setScale(scaleFactor.x, scaleFactor.y);

	Texture textureBranch;
	textureBranch.loadFromFile("graphics/branch.png");
	for (int i = 0; i < NUM_BRANCHES; i++) {
		branches[i].setTexture(textureBranch);
		branches[i].setPosition(-2000 * scaleFactor.x, -2000 * scaleFactor.y);
		branches[i].setOrigin(220, 20);
		branches[i].setScale(scaleFactor.x, scaleFactor.y);
	}

	Texture texturePlayer;
	texturePlayer.loadFromFile("graphics/player.png");
	Sprite spritePlayer;
	spritePlayer.setTexture(texturePlayer);
	spritePlayer.setPosition(580 * scaleFactor.x, 720 * scaleFactor.y);
	spritePlayer.setScale(scaleFactor.x, scaleFactor.y);

	side playerSide = side::LEFT;

	Texture textureRIP;
	textureRIP.loadFromFile("graphics/rip.png");
	Sprite spriteRIP;
	spriteRIP.setTexture(textureRIP);
	spriteRIP.setPosition(600 * scaleFactor.x, 860 * scaleFactor.y);
	spriteRIP.setScale(scaleFactor.x, scaleFactor.y);

	Texture textureAxe;
	textureAxe.loadFromFile("graphics/axe.png");
	Sprite spriteAxe;
	spriteAxe.setTexture(textureAxe);
	spriteAxe.setPosition(700 * scaleFactor.x, 830 * scaleFactor.y);
	spriteAxe.setScale(scaleFactor.x, scaleFactor.y);

	const float AXE_POSITION_LEFT = 700 * scaleFactor.x;
	const float AXE_POSITION_RIGHT = 1075 * scaleFactor.x;

	Texture textureLog;
	textureLog.loadFromFile("graphics/log.png");
	Sprite spriteLog;
	spriteLog.setTexture(textureLog);
	spriteLog.setPosition(810 * scaleFactor.x, 720 * scaleFactor.y);
	spriteLog.setScale(scaleFactor.x, scaleFactor.y);

	bool logActive = false;
	float logSpeedX = 1000;
	float logSpeedY = -1500;

	bool acceptInput = false;

	SoundBuffer chopBuffer;
	chopBuffer.loadFromFile("sound/chop.wav");
	Sound chop;
	chop.setBuffer(chopBuffer);

	/*
	Game Loop
	*/

	while (window.isOpen()) {
		/*
			Handle Player Input
		*/
		Event event;
		while (window.pollEvent(event)) {
			if (event.type == Event::KeyReleased && !paused) {
				acceptInput = true;
				spriteAxe.setPosition(2000 * scaleFactor.x, spriteAxe.getPosition().y * scaleFactor.y);
			}
		}

		if (Keyboard::isKeyPressed(Keyboard::Escape)) {
			window.close();
		}

		if (Keyboard::isKeyPressed(Keyboard::Return)) {
			paused = false;
			score = 0;
			timeRemaining = 5;

			for (int i = 1; i< NUM_BRANCHES; i++) {
				branchPositions[i] = side::NONE;
			}

			spriteRIP.setPosition(675 * scaleFactor.x, 2000 * scaleFactor.y);

			spritePlayer.setPosition(580 * scaleFactor.x, 720 * scaleFactor.y);
			acceptInput = true;
		}

		if (acceptInput) {
			if (Keyboard::isKeyPressed(Keyboard::Right)) {
				playerSide = side::RIGHT;
				spritePlayer.setPosition(1200 * scaleFactor.x, 720 * scaleFactor.y);
				score++;
				timeRemaining += (2 / score) + 0.15f;
				spriteAxe.setPosition(AXE_POSITION_RIGHT, 830 * scaleFactor.y);
				updateBranches(score);
				spriteLog.setPosition(810 * scaleFactor.x, 720 * scaleFactor.y);
				logSpeedX = -5000;
				logActive = true;
				acceptInput = false;
				chop.play();
			}
			if (Keyboard::isKeyPressed(Keyboard::Left)) {
				playerSide = side::LEFT;
				spritePlayer.setPosition(580 * scaleFactor.x, 720 * scaleFactor.y);
				score++;
				timeRemaining += (2 / score) + 0.15f;
				spriteAxe.setPosition(AXE_POSITION_LEFT, 830 * scaleFactor.y);
				updateBranches(score);
				spriteLog.setPosition(810 * scaleFactor.x, 720 * scaleFactor.y);
				logSpeedX = 5000;
				logActive = true;
				acceptInput = false;
				chop.play();
			}
		}

		/*
			Update Scene
		*/
		if (!paused) {
			Time deltaTime = clock.restart();

			timeRemaining -= deltaTime.asSeconds();
			timeBar.setSize(Vector2f(timeBarWidthPerSecond * timeRemaining, timeBarHeight));

			if (timeRemaining <= 0.0f) {
				paused = true;
				messageText.setString("Out of Time!");
				FloatRect textRect = messageText.getLocalBounds();
				messageText.setOrigin(
					textRect.left + textRect.width / 2.0f,
					textRect.top + textRect.height / 2.0f);
				messageText.setPosition(resolution.x / 2.0f, resolution.y / 2.0f);
				scoreText.setPosition(20, 20);
			}

			if (!beeActive) {
				// Set Speed
				srand((int)time(0) * 10);
				beeSpeed = (float)(rand() % 500) + 200;
				float factor = resolution.y / 2;
				// Set Bee Height
				srand((int)time(0) * 10);
				float height = (float)((rand() % (int)factor) + 100);
				spriteBee.setPosition(2000 * scaleFactor.x, height * scaleFactor.y);
				beeActive = true;
			}
			else {
				spriteBee.setPosition(
					spriteBee.getPosition().x - (beeSpeed * deltaTime.asSeconds()),
					spriteBee.getPosition().y
				);
				if (spriteBee.getPosition().x < -100) {
					beeActive = false;
				}
			}
			if (!cloud1Active) {
				srand((int)time(0) * 10);
				cloud1Speed = (rand() % 200 + 50);

				srand((int)time(0) * 10);
				float height = (rand() % 100);
				spriteCloud1.setPosition(-200 * scaleFactor.x, height * scaleFactor.y);
				cloud1Active = true;
			}
			else {
				spriteCloud1.setPosition(
					spriteCloud1.getPosition().x + (cloud1Speed * deltaTime.asSeconds()),
					spriteCloud1.getPosition().y
				);
				if (spriteCloud1.getPosition().x > resolution.x) {
					cloud1Active = false;
				}
			}
			if (!cloud2Active) {
				srand((int)time(0) * 20);
				cloud2Speed = (rand() % 200 + 50);

				srand((int)time(0) * 20);
				float height = (rand() % 300);
				spriteCloud2.setPosition(-200 * scaleFactor.x, height * scaleFactor.y);
				cloud2Active = true;
			}
			else {
				spriteCloud2.setPosition(
					spriteCloud2.getPosition().x + (cloud2Speed * deltaTime.asSeconds()),
					spriteCloud2.getPosition().y
				);
				if (spriteCloud2.getPosition().x > resolution.x) {
					cloud2Active = false;
				}
			}
			if (!cloud3Active) {
				srand((int)time(0) * 30);
				cloud3Speed = (rand() % 200 + 50);

				srand((int)time(0) * 30);
				float height = (rand() % 500);
				spriteCloud3.setPosition(-200 * scaleFactor.x, height * scaleFactor.y);
				cloud3Active = true;
			}
			else {
				spriteCloud3.setPosition(
					spriteCloud3.getPosition().x + (cloud3Speed * deltaTime.asSeconds()),
					spriteCloud3.getPosition().y
				);
				if (spriteCloud3.getPosition().x > resolution.x) {
					cloud3Active = false;
				}
			}
			std::stringstream ss;
			ss << "Score: " << score;
			scoreText.setString(ss.str());

			for (int i = 0; i < NUM_BRANCHES; i++) {
				float height = i * 150;
				if (branchPositions[i] == side::LEFT) {
					branches[i].setPosition(610 * scaleFactor.x, height * scaleFactor.y);
					branches[i].setRotation(180);
				}
				else if (branchPositions[i] == side::RIGHT) {
					branches[i].setPosition(1330 * scaleFactor.x, height* scaleFactor.y);
					branches[i].setRotation(0);
				}
				else {
					branches[i].setPosition(3000 * scaleFactor.x, height * scaleFactor.y);
				}
			}

			if (logActive) {
				spriteLog.setPosition(
					spriteLog.getPosition().x + (logSpeedX * deltaTime.asSeconds()),
					spriteLog.getPosition().y + (logSpeedY * deltaTime.asSeconds())
				);

				if (spriteLog.getPosition().x < -200 || spriteLog.getPosition().x > 2200) {
					logActive = false;
					spriteLog.setPosition(810 * scaleFactor.x, 720 * scaleFactor.y);
				}
			}

			if (branchPositions[5] == playerSide) {
				paused = true;
				acceptInput = false;

				if (playerSide == side::LEFT) {
					spriteRIP.setPosition(525 * scaleFactor.x, 760 * scaleFactor.y);
				} else {
					spriteRIP.setPosition(1200 * scaleFactor.x, 760 * scaleFactor.y);
				}
				spritePlayer.setPosition(2000 * scaleFactor.x, 600 * scaleFactor.y);

				messageText.setString("Squished!!");
				FloatRect textRect = messageText.getLocalBounds();
				messageText.setOrigin(
					textRect.left + textRect.width / 2.0f,
					textRect.top + textRect.height / 2.0f);
				messageText.setPosition(resolution.x / 2.0f, resolution.y / 2.0f);
			}
		}

		/*
			Draw Scene
		*/
		window.clear();
		window.draw(spriteBackground);

		window.draw(spriteCloud1);
		window.draw(spriteCloud2);
		window.draw(spriteCloud3);

		for (int i = 0; i < NUM_BRANCHES; i++) {
			window.draw(branches[i]);
		}
		window.draw(spriteTree);

		window.draw(spritePlayer);
		window.draw(spriteAxe);
		window.draw(spriteLog);
		window.draw(spriteRIP);

		window.draw(spriteBee);

		window.draw(scoreText);
		if (paused) {
			window.draw(messageText);
		}

		window.draw(timeBar);

		window.display();
	}

    return 0;
}

void updateBranches(int seed) {
	for (int j = NUM_BRANCHES - 1; j > 0; j--) {
		branchPositions[j] = branchPositions[j - 1];
	}
	srand((int)time(0) + seed);
	int r = (rand() % 5);
	switch (r) {
		case 0:
			branchPositions[0] = side::LEFT;
			break;
		case 1:
			branchPositions[0] = side::RIGHT;
			break;
		default:
			branchPositions[0] = side::NONE;
			break;
	}
}